package com.zuitt.example;

import java.util.Scanner;
public class Person {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("First Name : ");
        String firstname = scanner.nextLine();

        System.out.println("Last Name : ");
        String lastname = scanner.nextLine();

        System.out.println("First Subject grade :");
        float firstGrade = scanner.nextFloat();

        System.out.println("Second Subject grade :");
        float secondGrade = scanner.nextFloat();

        System.out.println("Third Subject grade :");
        float thirdGrade = scanner.nextFloat();

        System.out.println("Good Day " + firstname + " " + lastname + " !");

        int averageGrade = (int) ((firstGrade + secondGrade + thirdGrade) / 3);
        System.out.println("Your Average Grade is : " + averageGrade );
    }
}
