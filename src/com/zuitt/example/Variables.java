package com.zuitt.example;

import javax.sound.midi.SysexMessage;

public class Variables {
    public static void main(String[] args) {
        int age;
        char middleName;

        /* Variable Declaration vs. Initialization*/
        // Initialization
        int x;
        //With Declaration
        int y = 0;

        //after declaration
        x = 1;

        System.out.println("y value: " + y + ", x value: " + x);

        /* Primitive Data Types */
        //int
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        long longNumber = 1000000000000000000L;
        System.out.println(longNumber);

        //float
        float floatingNumber = 3.14159266535f;
        System.out.println(floatingNumber);

        //double
        double doubleNumber = 3.14159266535;
        System.out.println(doubleNumber);

        //char
        char letter = 'a';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        // Java uses the "final" keyword so the variable's value cannot be changed.
        final float PI = 3.14f;
        System.out.println(PI);

        /* Non-Primitive Data */
        //String is object
        String word = "Hello World!";
        System.out.println(word);

        //String Method
        int stringLength = word.length();
        System.out.println(stringLength);

    }
}
